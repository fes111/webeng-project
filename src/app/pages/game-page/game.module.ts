import { GameComponentComponent } from './../../components/game-component/game-component.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameRoutingModule } from './game-routing.module';
import { GamePageComponent } from './game.component';
@NgModule({
  declarations: [GamePageComponent, GameComponentComponent],
  imports: [CommonModule, GameRoutingModule]
})
export class GameModule {}
