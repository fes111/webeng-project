import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RegisterModalComponent, Data } from 'src/app/components/register-modal/register-modal.component';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GamePageComponent implements OnInit {
  constructor(private modalService: NgbModal, private router: Router, private toatr: ToastrService) {}

  async ngOnInit() {
    let user = JSON.parse(localStorage.getItem('user'));
    const modalRef = this.modalService.open(RegisterModalComponent, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    if (user) {
      modalRef.componentInstance.data = user;
      modalRef.componentInstance.validate(user);
    }
    const response: Data = await modalRef.result;

    localStorage.setItem('user', JSON.stringify(response));
  }
}
