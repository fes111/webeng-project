import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'game-component',
  templateUrl: './game-component.component.html',
  styleUrls: ['./game-component.component.scss']
})
export class GameComponentComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor() {}

  @ViewChild('canvas', { static: false })
  canvas: ElementRef<HTMLCanvasElement>;

  public ctx: CanvasRenderingContext2D;
  public height: number = window.innerHeight * 0.45;
  public width: number = window.innerWidth * 0.6;
  public interval: number;
  public time: number = 0;
  public amount: number = 10000;
  public course: number = 100;
  public shares: number = 0;
  public totalShares: number = 1000;
  public cap: number = 500;
  public ups: number = 0;
  public isRunning: boolean = false;
  public downs: number = 0;
  public moves = [];
  private lastUpAmount: number = 0;
  private lastDownAmount: number = 0;
  private timeSpan: number = 1000;
  private fiveMinutes = this.timeSpan * 60 * 5;
  public timeLeft = this.fiveMinutes / 1000;

  public profitAmout: number = 0;
  public profitProsent: number = 0;

  ngAfterViewInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.canvas.nativeElement.width = this.width;
    this.canvas.nativeElement.height = this.height;
    this.canvas.nativeElement.style.backgroundColor = 'black';
    this.ctx.strokeStyle = '#ffff';
    this.ctx.fillStyle = '#ffff';
  }
  ngOnDestroy() {
    this.stop();
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (this.isRunning) {
      if (event.key === 'ArrowUp') {
        this.draw(40);
      } else if (event.key === 'ArrowDown') {
        this.draw(10);
      }
    }
  }

  public stop(): void {
    clearInterval(this.interval);
    this.isRunning = false;
  }

  public start(): void {
    if (!this.isRunning) {
      this.ctx.clearRect(0, 0, this.width, this.height);
      this.time = 0;
      this.isRunning = true;
      this.timeLeft = this.fiveMinutes / 1000;
      this.amount = 10000;
      this.course = 100;
      this.shares = 0;
      this.totalShares = 1000;
      this.drawScale();
      this.interval = setInterval(() => {
        this.gameLoop();
      }, this.timeSpan);
      setTimeout(() => {
        this.stop();
      }, this.fiveMinutes);
    }
  }

  public gameLoop(): any {
    this.draw();
  }

  private calcProfit(): void {
    const amount = this.amount + this.course * this.shares - 10000;
    const procent = (amount / 10000) * 100;
    this.profitAmout = amount;
    this.profitProsent = procent;
  }

  private draw(goesUp?: number) {
    this.timeLeft--;
    let oldTime = this.time;
    const oldCourse = this.course;
    if (this.timeLeft <= 0) {
      this.stop();
    }
    if (this.time >= this.width - 50) {
      this.time = 0;
      this.ctx.clearRect(0, 0, this.width, this.height);
      this.moves = [];
      this.drawScale();
    }
    oldTime = this.time;
    this.time += 10;
    if (!goesUp) {
      goesUp = this.goesUp();
    }
    if (goesUp < 25 && this.course > 0) {
      // 25%
      this.down(0.9);
    } else if (goesUp > 30 && goesUp <= 60) {
      // 30%
      this.up(1.1);
    } else if (goesUp > 72 && goesUp <= 80) {
      // 8%
      this.up(1.3);
    } else if (goesUp > 92 && goesUp <= 100) {
      // 8%
      this.down(0.7);
    } else if (this.ups >= 3 && goesUp < 75) {
      // 75%
      this.up(this.lastUpAmount);
    } else if (this.ups >= 3 && goesUp > 75) {
      // 75%
      this.down(this.lastDownAmount);
    } else if (this.downs >= 3 && goesUp < 75) {
      // 75%
      this.down(this.lastDownAmount);
    } else if (this.downs >= 3 && goesUp > 75) {
      // 75%
      this.up(this.lastUpAmount);
    }
    this.calcProfit();
    this.moves.push([oldTime + 50, oldCourse, this.time + 50, this.course]);

    this.line(oldTime + 50, this.height - (this.height / this.cap) * oldCourse, this.time + 50, this.height - (this.height / this.cap) * this.course);
  }

  private redraw() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.drawScale();
    for (const move of this.moves) {
      this.line(move[0], this.height - (this.height / this.cap) * move[1], move[2], this.height - (this.height / this.cap) * move[3]);
    }
  }

  private up(amount: number) {
    this.lastUpAmount = amount;
    this.course = this.course * amount;
    this.ups += 1;
    this.downs = 0;
    if (this.course >= this.cap) {
      this.cap = this.cap * 2;
      this.redraw();
    }
  }

  private down(amount: number) {
    this.lastDownAmount = amount;
    this.course = this.course * amount;
    this.downs += 1;
    this.ups = 0;
    if (this.course <= this.cap / 2 && this.cap > 500) {
      this.cap = this.cap * 0.5;
      this.redraw();
    }
  }

  public buy(amount: number | string) {
    if (this.isRunning) {
      if (typeof amount == 'string') amount = parseInt(amount);
      const price = this.course * amount;
      if (price <= this.amount) {
        this.amount -= price;
        this.shares += amount;
      }
    }
  }

  public sell(amount: number | string) {
    if (this.isRunning) {
      if (typeof amount == 'string') amount = parseInt(amount);
      const price = this.course * amount;
      this.amount += price;
      this.shares -= amount;
    }
  }

  public goesUp(): number {
    return Math.floor(Math.random() * 100);
  }

  public drawScale(): void {
    const scale = this.cap / 5;
    this.line(50, 0, 50, this.height, 1);
    for (let i = 1; i <= 5; i++) {
      this.text(3, this.height - (this.height / this.cap) * scale * i + 20, scale * i);
      this.line(0, this.height - (this.height / this.cap) * scale * i, this.width, this.height - (this.height / this.cap) * scale * i, 1);
    }
  }

  public text(x, y, text): void {
    this.ctx.font = '20px Arial';
    this.ctx.fillText(text, x, y);
  }

  public line(x1: number, y1: number, x2: number, y2: number, width: number = 2): void {
    this.ctx.lineWidth = width;
    this.ctx.beginPath();
    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.stroke();
  }

  ngOnInit(): void {}
}
