import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
export interface Data {
  firstName: string;
  lastName: string;
  email: string;
  birthday: NgbDateStruct;
  title: string;
  plz: number;
}

@Component({
  selector: 'app-adress-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss']
})
export class RegisterModalComponent implements OnInit {
  public data: Data = {
    firstName: '',
    lastName: '',
    email: '',
    plz: 0,
    birthday: {
      year: new Date().getFullYear(),
      month: new Date().getMonth(),
      day: new Date().getDay()
    },
    title: 'Herr'
  };

  public minDate: Date = new Date(Date.UTC(1950, 0));
  public valids: { [key: string]: boolean } = {
    first: false,
    last: false,
    title: true,
    email: false,
    plz: false,
    date: false
  };

  constructor(private activeModal: NgbActiveModal, private router: Router) {}

  public validate(data: Data): boolean {
    this.valids = {
      first: true,
      last: true,
      title: true,
      email: true,
      plz: true,
      date: true
    };
    if (!data.firstName || data.firstName.length < 3) {
      this.valids.first = false;
    }
    if (!data.lastName || data.lastName.length < 3) {
      this.valids.last = false;
    }
    if (!data.title) {
      this.valids.title = false;
    }
    if (!data.email || !this.validateEmail(data.email)) {
      this.valids.email = false;
    }
    if (!data.plz || data.plz.toString().length != 5) {
      this.valids.plz = false;
    }
    if (!data.birthday || !this.isDate18orMoreYearsOld(data.birthday.day, data.birthday.month, data.birthday.year)) {
      this.valids.date = false;
    }

    if (this.valids.first && this.valids.last && this.valids.title && this.valids.email && this.valids.plz && this.valids.date) {
      return true;
    } else {
      return false;
    }
  }
  private isDate18orMoreYearsOld(day, month, year) {
    return new Date(year + 18, month - 1, day) <= new Date();
  }

  private validateEmail(email: string) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  public close() {
    this.activeModal.close(this.data);
  }
  public submit() {
    if (this.validate(this.data)) {
      this.activeModal.close(this.data);
    }
  }

  ngOnInit(): void {}
}
