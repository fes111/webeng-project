# Webeng Projekt

simon.a.frank@gmail.com

---

## Anforderungen

NodeJS & npm müssen installiert sein und im Systempfad stehen

## Installierung

    $ git clone https://gitlab.com/fes111/webeng-project.git
    $ cd webeng-project
    $ npm install

## Projekt starten

    $ npm start

## Build für deployment

    $ npm build

@FeS111
